import chroma from 'chroma-js';

const leftPad = (value: number, totalWidth: number, paddingChar?: string) => {
  var length = totalWidth - value.toString().length + 1;
  return Array(length).join(paddingChar || '0') + value;
};

const rangeOfColors = (
  initialColor: string,
  finalColor: string,
  length: number,
): Array<string> =>
  chroma.scale([initialColor, finalColor]).mode('lch').colors(length);

const addAlpha = (color: string, opacity: number): string => {
  // coerce values so ti is between 0 and 1.
  const _opacity = Math.round(Math.min(Math.max(opacity || 1, 0), 1) * 255);
  return color + _opacity.toString(16).toUpperCase();
};

export { leftPad, rangeOfColors, addAlpha };
