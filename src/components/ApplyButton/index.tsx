import React, { FC } from 'react';
import {
  GestureResponderEvent,
  Text,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';

import styles from './ApplyButton.styles';

import { Gradient } from '../Gradient';

interface IApplyButtonProps {
  onPress?: ((event: GestureResponderEvent) => void) & (() => void);
  title: string;
  style?: ViewStyle;
}

export const ApplyButton: FC<IApplyButtonProps> = ({
  onPress,
  title,
  style,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={[styles.container, style]}>
      <Gradient
        style={styles.gradient}
        cornerRadius={24}
        colors={['#4a54df', '#15d4d8']}>
        <Text style={styles.buttonTitle}>{title}</Text>
      </Gradient>
    </TouchableOpacity>
  );
};
