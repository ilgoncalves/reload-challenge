import { ViewStyle, StyleSheet, TextStyle } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

interface Style {
  container: ViewStyle;
  buttonTitle: TextStyle;
  gradient: ViewStyle;
}

export default StyleSheet.create<Style>({
  container: {
    height: hp(6),
    borderRadius: hp(6),
    marginHorizontal: wp(8),
    shadowColor: '#000',
    shadowOffset: {
      width: -1,
      height: 3,
    },
    shadowOpacity: 0.2,
    shadowRadius: 3,

    elevation: 6,
  },
  gradient: {
    width: '100%',
    height: '100%',
    borderRadius: hp(6),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTitle: {
    color: '#FFF',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
