import { ViewStyle, StyleSheet, ImageStyle } from 'react-native';

interface Style {
  container: ViewStyle;
  icon: ImageStyle;
  notificationBadge: ViewStyle;
}

export default StyleSheet.create<Style>({
  container: {
    width: 34,
    height: 34,
    padding: 2,
  },
  icon: {
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  notificationBadge: {
    position: 'absolute',
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: '#e87454',
    right: 0,
    top: 0,
    zIndex: 10,
  },
});
