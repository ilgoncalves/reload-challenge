import React, { FC } from 'react';
import { View, Image, GestureResponderEvent } from 'react-native';
import styles from './ChatButton.styles';

import ChatIcon from '../../assets/images/chat_icon.png';
import { TouchableOpacity } from 'react-native-gesture-handler';

interface IChatButtonProps {
  onPress?: ((event: GestureResponderEvent) => void) & (() => void);
}

export const ChatButton: FC<IChatButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View style={styles.notificationBadge} />

      {/* @ts-ignore */}
      <Image source={ChatIcon} style={styles.icon} />
    </TouchableOpacity>
  );
};
