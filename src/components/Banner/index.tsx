import React, { FC } from 'react';
import { ImageBackground, Text } from 'react-native';
import LifeStyleImage from '../../assets/images/ilustration_lifestyle.png';
import styles from './Banner.styles';

interface IBannerProps {}

export const Banner: FC<IBannerProps> = ({}) => {
  return (
    //@ts-ignore
    <ImageBackground style={styles.container} source={LifeStyleImage}>
      <Text style={styles.title}>Lifestyle</Text>

      <Text style={styles.description}>
        Get a holistic view of your activities to enhance your wellbeing and
        benefit from even more accurate recommendations.
      </Text>
    </ImageBackground>
  );
};
