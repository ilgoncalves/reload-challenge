import { ViewStyle, StyleSheet, TextStyle } from 'react-native';

import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

interface Style {
  container: ViewStyle;
  title: TextStyle;
  description: TextStyle;
}

export default StyleSheet.create<Style>({
  container: {
    height: 180,
    resizeMode: 'contain',

    paddingLeft: 16,
    paddingTop: 16,
  },
  title: {
    fontFamily: 'Roboto',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },
  description: {
    fontFamily: 'Roboto',
    color: '#fff',
    fontWeight: '300',
    fontSize: 12,
    lineHeight: 18,
    width: wp(39),
    marginTop: 8,
  },
});
