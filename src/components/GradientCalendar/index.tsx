import React, { FC, useState } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

import ArrowLeft from '../../assets/svgs/arrow-left-solid.svg';
import ArrowRight from '../../assets/svgs/arrow-right-solid.svg';

import styles from './GradientCalendar.styles';
import { addAlpha, leftPad, rangeOfColors } from '../../utils';
import { DayComponent } from '../DayComponent';

interface IGradientCalendarProps {
  onSelectRange?: (initialDate: string, finalDate: string) => void;
}
const weekDays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

const numberOfDaysByMonthId: { [monthId: number]: number } = {
  0: 31,
  1: 28,
  2: 31,
  3: 30,
  4: 31,
  5: 30,
  6: 31,
  7: 31,
  8: 30,
  9: 31,
  10: 30,
  11: 31,
};

export const GradientCalendar: FC<IGradientCalendarProps> = ({
  onSelectRange,
}) => {
  const [currentMonth, setCurrentMonth] = useState(new Date().getMonth());
  const [currentYear, setCurrentYear] = useState(new Date().getFullYear());

  const [daysSelected, setDaysSelected] = useState<{
    initialDay: string | null;
    finalDay: string | null;
  }>({
    initialDay: null,
    finalDay: null,
  });

  const getInitialMonthDay = (date: string) => {
    var arr: any = date.split('/').reverse();
    return new Date(arr[0], arr[1] - 1, arr[2]).getDay();
  };

  const daysSlots = Array(49)
    .fill(null)
    .map((_, i) => i + 1);

  const renderMonth = (
    startAt: number,
    numberOfDays: number,
    initialDaySelected: string | null,
    finalDaySelected: string | null,
  ) => {
    let currentDayIndex = 0;
    let diffOfDays = 0;
    let colors: Array<string> = [];
    if (initialDaySelected && finalDaySelected) {
      diffOfDays =
        parseInt(finalDaySelected, 10) - parseInt(initialDaySelected, 10) + 1;
      colors = rangeOfColors(
        addAlpha('#15d4d8', 0.11),
        addAlpha('#4a54df', 0.25),
        diffOfDays,
      );
    }

    return daysSlots.map(slot => {
      let linearColors = ['transparent', 'transparent'];
      let toDisplay = slot <= 7 ? weekDays[slot - 1] : '';

      const firstDay = 7 + startAt;

      if (slot > 7 && currentDayIndex < numberOfDays) {
        if (slot > firstDay) {
          currentDayIndex++;
          toDisplay = currentDayIndex.toString();
        }
      }

      const isInvalidDay = Number.isNaN(parseInt(toDisplay, 10));

      let isTheLastOne: boolean = false;
      let isTheFirstOne: boolean = false;
      if (
        !isInvalidDay &&
        initialDaySelected &&
        finalDaySelected &&
        currentDayIndex > 0 &&
        currentDayIndex >= parseInt(initialDaySelected, 10) &&
        currentDayIndex <= parseInt(finalDaySelected, 10)
      ) {
        isTheLastOne = currentDayIndex === parseInt(finalDaySelected, 10);
        isTheFirstOne = currentDayIndex === parseInt(initialDaySelected, 10);
        const indexOfColor = currentDayIndex - parseInt(initialDaySelected, 10);
        const color = colors[indexOfColor];
        const lastColor = isTheLastOne ? color : colors[indexOfColor + 1];
        linearColors = [color, lastColor];
      }

      const isCurrentSelected = daysSelected.initialDay === toDisplay;

      return (
        <DayComponent
          dayValue={toDisplay}
          isFirstSelection={isTheFirstOne}
          isLastSelection={isTheLastOne}
          isNotADay={isInvalidDay}
          linearColors={linearColors}
          onPress={async () => handleDayPress(toDisplay)}
          selected={isCurrentSelected}
        />
      );
    });
  };

  const mountDate = (day: string) => {
    return `${currentYear}-${leftPad(currentMonth + 1, 2)}-${leftPad(
      parseInt(day, 10),
      2,
    )}`;
  };

  const handleDayPress = (dayPressed: string) => {
    const { finalDay, initialDay } = daysSelected;
    if (!initialDay || (initialDay && finalDay)) {
      return setDaysSelected({ initialDay: dayPressed, finalDay: null });
    }
    if (!finalDay) {
      onSelectRange &&
        onSelectRange(mountDate(initialDay), mountDate(dayPressed));
      return setDaysSelected({ ...daysSelected, finalDay: dayPressed });
    }
  };

  const onMonthChange = (signal: number) => {
    setDaysSelected({
      finalDay: null,
      initialDay: null,
    });
    if (
      (currentMonth === 0 && signal === -1) ||
      (currentMonth === 11 && signal === 1)
    ) {
      setCurrentYear(prev => prev + 1 * signal);
      return setCurrentMonth(signal > 0 ? 0 : 11);
    }
    setCurrentMonth(prev => prev + 1 * signal);
  };

  return (
    <View>
      <View style={styles.controlsWrapper}>
        <TouchableOpacity onPress={() => onMonthChange(-1)}>
          <ArrowLeft width={20} height={20} fill={addAlpha('#000000', 0.25)} />
        </TouchableOpacity>
        <Text style={styles.controlsText}>
          {`${currentYear} - ${months[currentMonth]}`}
        </Text>
        <TouchableOpacity onPress={() => onMonthChange(1)}>
          <ArrowRight width={20} height={20} fill={addAlpha('#000000', 0.25)} />
        </TouchableOpacity>
      </View>

      <View style={styles.divider} />
      <View style={styles.calendar}>
        {renderMonth(
          getInitialMonthDay(
            `01/${leftPad(currentMonth + 1, 2)}/${currentYear}`,
          ),
          numberOfDaysByMonthId[currentMonth],
          daysSelected.initialDay,
          daysSelected.finalDay,
        )}
      </View>
    </View>
  );
};
