import { ViewStyle, StyleSheet, TextStyle } from 'react-native';

import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { addAlpha } from '../../utils';

interface Style {
  controlsText: TextStyle;
  calendar: ViewStyle;
  divider: ViewStyle;
  controlsWrapper: ViewStyle;
}

export default StyleSheet.create<Style>({
  controlsText: {
    fontSize: 16,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    color: addAlpha('#000000', 0.4),
  },
  controlsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 20,
    marginHorizontal: 18,
  },
  divider: {
    marginHorizontal: 18,
    height: hp(0.2),
    marginBottom: 8,
    borderRadius: 1,
    backgroundColor: addAlpha('#000000', 0.2),
  },
  calendar: {
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
});
