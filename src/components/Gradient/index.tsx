import React, { FC } from 'react';
import { ViewStyle } from 'react-native';
//@ts-ignore
import GradientNative from './GradientNative';

interface IGradientProps {
  style: ViewStyle;
  colors: Array<string>;
  cornerRadius?: number;
  progress?: number;
}

export const Gradient: FC<IGradientProps> = ({
  children,
  colors,
  style,
  cornerRadius,
}) => {
  return (
    <GradientNative
      cornerRadius={cornerRadius}
      progress={1}
      style={style}
      fromColor={colors[0]}
      toColor={colors[1]}>
      {children}
    </GradientNative>
  );
};
