import React, { FC } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from './DayComponent.styles';

import { leftPad } from '../../utils';
import { Gradient } from '../Gradient';

interface IDayComponentProps {
  linearColors: Array<string>;
  selected: boolean;
  isFirstSelection: boolean;
  isLastSelection: boolean;
  isNotADay: boolean;
  dayValue: string;
  onPress: () => {};
}

interface IDayContentProps {
  onPress: () => {};
  isNotADay: boolean;
  disabled: boolean;
  selected: boolean;
}

export const DayContent: FC<IDayContentProps> = ({
  onPress,
  isNotADay,
  disabled,
  selected,
  children,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={disabled}
      style={[styles.day, selected && styles.daySelected]}>
      <Text
        style={[
          styles.dayText,
          isNotADay && styles.dayNameText,
          selected && styles.selectedDayText,
        ]}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

export const DayComponent: FC<IDayComponentProps> = ({
  linearColors,
  selected,
  onPress,
  isFirstSelection,
  isLastSelection,
  isNotADay,
  dayValue,
}) => {
  return (
    <View style={styles.dayWrapper}>
      {isFirstSelection && (
        <View
          style={[styles.leftRadius, { backgroundColor: linearColors[0] }]}
        />
      )}
      <Gradient
        colors={linearColors}
        //@ts-ignore
        style={[
          styles.gradient,
          (isFirstSelection || isLastSelection) && { width: '78%' },
        ]}>
        <DayContent
          onPress={onPress}
          disabled={isNotADay || selected}
          selected={selected}
          isNotADay={isNotADay}>
          {!isNotADay && parseInt(dayValue, 10) < 10
            ? leftPad(parseInt(dayValue, 10), 2)
            : dayValue}
        </DayContent>
      </Gradient>
      {isLastSelection && (
        <View
          style={[styles.rightRadius, { backgroundColor: linearColors[1] }]}
        />
      )}
    </View>
  );
};
