import { ViewStyle, StyleSheet, TextStyle } from 'react-native';

import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

interface Style {
  day: ViewStyle;
  daySelected: ViewStyle;
  dayText: TextStyle;
  dayNameText: TextStyle;
  selectedDayText: TextStyle;
  dayWrapper: ViewStyle;
  gradient: ViewStyle;
  rightRadius: ViewStyle;
  leftRadius: ViewStyle;
}

export default StyleSheet.create<Style>({
  leftRadius: {
    borderTopLeftRadius: 200,
    borderBottomLeftRadius: 200,
    width: 12,
    height: '70%',
  },
  rightRadius: {
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    width: 12,
    height: '70%',
  },
  selectedDayText: {
    color: '#FFF',
    fontSize: 15,
    fontWeight: '600',
    fontFamily: 'Roboto',
  },
  dayText: {
    fontSize: 15,
    fontFamily: 'Roboto',
  },

  dayNameText: {
    fontWeight: 'bold',
    fontSize: 12,
  },
  day: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dayWrapper: {
    position: 'relative',
    flex: 1,
    flexDirection: 'row',
    height: hp(5),
    marginVertical: hp(0.5),
    flexBasis: '14%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  gradient: {
    width: '100%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  daySelected: {
    backgroundColor: '#15d4d8',
    zIndex: 1000,
    width: hp(4.2),
    height: hp(4.2),
    borderRadius: hp(4.2),
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
