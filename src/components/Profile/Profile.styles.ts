import { ViewStyle, StyleSheet, ImageStyle } from 'react-native';

interface Style {
  container: ViewStyle;
  imageContainer: ViewStyle;
  image: ImageStyle;
  gradient: ViewStyle;
  icon: ViewStyle;
}

export default StyleSheet.create<Style>({
  container: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: -1,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3,

    elevation: 5,
    position: 'relative',
  },
  imageContainer: {},
  image: {
    height: 36,
    width: 36,

    borderRadius: 18,
  },
  icon: {
    transform: [{ rotate: '90deg' }],
  },
  gradient: {
    width: 20,
    height: 20,
    transform: [{ rotate: '90deg' }],
    borderRadius: 10,
    position: 'absolute',
    bottom: -3,
    right: -6,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
