import React, { FC } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import styles from './Profile.styles';

//mock photo
import DefaultProfilePhoto from '../../assets/images/user.jpg';
import MenuIcon from '../../assets/svgs/menu.svg';
import { Gradient } from '../Gradient';

interface IProfileProps {}

export const Profile: FC<IProfileProps> = ({}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.imageContainer}>
        {/* @ts-ignore */}
        <Image source={DefaultProfilePhoto} style={styles.image} />
        <Gradient
          cornerRadius={10}
          style={styles.gradient}
          colors={['#4a54df', '#15d4d8']}>
          <View style={styles.icon}>
            <MenuIcon width={11} height={11} fill={'#FFF'} />
          </View>
        </Gradient>
      </View>
    </TouchableOpacity>
  );
};
