import { ViewStyle, StyleSheet } from 'react-native';

interface Style {
  container: ViewStyle;
}

export default StyleSheet.create<Style>({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginBottom: 12,
  },
});
