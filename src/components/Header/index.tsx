import React, { FC } from 'react';
import { SafeAreaView, View } from 'react-native';
import styles from './Header.styles';

import Logo from '../../assets/svgs/logo-black.svg';
import { ChatButton } from '../ChatButton';
import { Profile } from '../Profile';

interface IHeaderProps {}

export const Header: FC<IHeaderProps> = ({}) => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Profile />
        <Logo width={90} height={10.03} />
        <ChatButton />
      </View>
    </SafeAreaView>
  );
};
