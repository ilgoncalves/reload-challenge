import React, { FC, useState } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import {
  ApplyButton,
  Banner,
  GradientCalendar,
  Header,
  SlideModal,
} from './components';

const App: FC = () => {
  const [isModalOpened, setIsModalOpened] = useState(false);
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Header />
      <Banner />
      <ApplyButton
        style={styles.scheduleButton}
        onPress={() => setIsModalOpened(true)}
        title="Schedule"
      />
      <SlideModal
        isVisible={isModalOpened}
        dismiss={() => setIsModalOpened(false)}>
        <View style={styles.modalContent}>
          <GradientCalendar />
          <ApplyButton onPress={() => setIsModalOpened(false)} title="Apply" />
        </View>
      </SlideModal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  modalContent: {
    flex: 1,
    marginHorizontal: wp(2),
  },
  scheduleButton: {
    marginVertical: 32,
  },
});

export default App;
