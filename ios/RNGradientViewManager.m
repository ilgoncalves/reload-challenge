#import "RNGradientViewManager.h"
#import "RNGradientView.h"
#import <React/RCTBridge.h>

@implementation RNGradientViewManager
RCT_EXPORT_MODULE()

@synthesize bridge = _bridge;

- (UIView *)view {
  return [[RNGradientView alloc] init];
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_VIEW_PROPERTY(progress, NSNumber);
RCT_EXPORT_VIEW_PROPERTY(cornerRadius, NSNumber);
RCT_EXPORT_VIEW_PROPERTY(fromColor, UIColor);
RCT_EXPORT_VIEW_PROPERTY(toColor, UIColor);
@end
